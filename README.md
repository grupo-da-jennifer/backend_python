# Precativos Backend

### Funcionalidades

O projeto ainda está em desenvolvimento e até o momento possui as seguintes funcionalidades

- [x] Cadastro completo de precatório
- [x] Cadastro completo de usuário pessoa física
- [x] Autenticação (Com JWT)
- [x] Ambiente de administrador

## 💻 Pré-requisitos

Antes de começar, verifique se você atendeu aos seguintes requisitos:

- Você instalou a versão mais recente de [Python 3](https://www.python.org/downloads/)
- Você instalou a versão mais recente de [pip](https://pip.pypa.io/en/stable/getting-started/)

## 🚀 Instalando precativos backend

Para instalar o **precativos backend**, siga estas etapas:

#### 1 - Criar o ambiente virtual  

`python -m venv <nomedavenv>`  

Exemplo:
`python -m venv venv`  


#### 2 – Ativar/Carregar o ambiente virtual  

`<nomedavenv>/scripts/activate`  

Exemplo:
`venv/scripts/activate`

#### 3 - Instalar as bibliotecas

`pip install -r requirements.txt`

#### 4 - Criando o banco(SQLite)

    python manage.py migrate
    
#### 5 - Criação de Superusuário

    python manage.py createsuperuser


## ☕ Usando precativos backend

Para usar **precativos backend**, siga estas etapas:

#### Executar api

    python manage.py runserver
   
## ☕ Links de consulta

    http://127.0.0.1:8000/api/swagger/ (localhost)

    http://127.0.0.1:8000/admin