argcomplete==3.3.0
asgiref==3.8.1
attrs==23.2.0
Babel==2.15.0
black==24.4.2
charset-normalizer==3.3.2
click==8.1.7
colorama==0.4.6
commitizen==3.27.0
decli==0.6.2
Django==5.0.6
django-cors-headers==4.4.0
django-money==3.5.2
djangorestframework==3.15.2
djangorestframework-simplejwt==5.3.1
drf-spectacular==0.27.2
importlib_metadata==7.2.1
inflection==0.5.1
isort==5.13.2
Jinja2==3.1.4
jsonschema==4.22.0
jsonschema-specifications==2023.12.1
MarkupSafe==2.1.5
mypy-extensions==1.0.0
packaging==24.1
pathspec==0.12.1
platformdirs==4.2.2
prompt-toolkit==3.0.36
py-moneyed==3.0
PyJWT==2.8.0
PyYAML==6.0.1
questionary==2.0.1
referencing==0.35.1
rpds-py==0.18.1
sqlparse==0.5.0
termcolor==2.4.0
tomli==2.0.1
tomlkit==0.12.5
typing_extensions==4.12.2
uritemplate==4.1.1
wcwidth==0.2.13
zipp==3.19.2
