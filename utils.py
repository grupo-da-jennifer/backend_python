ESFERA = [("Municipal", "municipal"), ("Estadual", "estadual"), ("Federal", "federal")]

SEXO = [("MASCULINO", "M"), ("FEMININO", "F"), ("OUTROS", "Não declarar")]

TIPO_LOGRADOURO = [
    ("Rua", "Rua"),
    ("Avenida", "Avenida"),
    ("Escadaria", "Escadaria"),
    ("Beco", "Beco"),
]
UF = [
    ("ES", "ES"),
    ("RJ", "RJ"),
    ("MG", "MG"),
    ("SP", "SP"),
    ("AC", "AC"),
    ("AL", "AL"),
    ("AP", "AP"),
    ("AM", "AM"),
    ("BA", "BA"),
    ("CE", "CE"),
    ("DF", "DF"),
    ("GO", "GO"),
    ("MA", "MA"),
    ("MT", "MT"),
    ("MS", "MS"),
    ("PA", "PA"),
    ("PB", "PB"),
    ("PR", "PR"),
    ("PE", "PE"),
    ("PI", "PI"),
    ("RJ", "RJ"),
    ("RN", "RN"),
    ("RS", "RS"),
    ("RO", "RO"),
    ("RR", "RR"),
    ("SC", "SC"),
    ("SE", "SE"),
    ("TO", "TO"),
]

NATUREZA = [
    ("Comum", "comum"),
    ("Previdenciária", "previdenciária"),
    ("Tributária", "tributária"),
    ("Alimentar", "alimentar"),
    ("Outros", "Outros"),
]

TIPO_USUARIO = [("pf", "Pessoa Física"), ("pj", "Pessoa Jurídica")]
