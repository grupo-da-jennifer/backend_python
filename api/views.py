import re
from rest_framework import viewsets, generics
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework_simplejwt.views import TokenObtainPairView
from rest_framework.mixins import (
    CreateModelMixin,
    ListModelMixin,
    RetrieveModelMixin,
    DestroyModelMixin,
    UpdateModelMixin,
)
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.viewsets import ModelViewSet, GenericViewSet
from api.serializers import (
    MyTokenObtainPairSerializer,
    PrecatorioSerializer,
    EnderecoSerializer,
    PessoaFisicaSerializer,
    PessoaJuridicaSerializer,
    EntidadePublicaSerializer,
)

from api.models import (
    Precatorio,
    Endereco,
    PessoaFisica,
    PessoaJuridica,
    EntidadePublica,
)


class PrecatorioViewSet(viewsets.ModelViewSet):
    queryset = Precatorio.objects.all()
    serializer_class = PrecatorioSerializer
    # permission_classes = [IsAuthenticated]


class EntidadePublicaViewSet(viewsets.ModelViewSet):
    queryset = EntidadePublica.objects.all()
    serializer_class = EntidadePublicaSerializer
    # permission_classes = [IsAuthenticated]


class EnderecoViewSet(viewsets.ModelViewSet):
    queryset = Endereco.objects.all()
    serializer_class = EnderecoSerializer

    # permission_classes = [IsAuthenticated]
    def create(self, request):
        request.data["cep"] = re.sub(r"\D", "", request.data["cep"])

        return super().create(request)


class PessoaFisicaViewSet(
    ListModelMixin,
    RetrieveModelMixin,
    DestroyModelMixin,
    UpdateModelMixin,
    GenericViewSet,
):

    queryset = PessoaFisica.objects.all()
    serializer_class = PessoaFisicaSerializer
    # permission_classes = [IsAuthenticated]


class PessoaJuridicaViewSet(
    ListModelMixin,
    RetrieveModelMixin,
    DestroyModelMixin,
    UpdateModelMixin,
    GenericViewSet,
):
    queryset = PessoaJuridica.objects.all()
    serializer_class = PessoaJuridicaSerializer
    # permission_classes = [IsAuthenticated]


class RegistarPessoaFisicaViewSet(CreateModelMixin, GenericViewSet):
    serializer_class = PessoaFisicaSerializer
    permission_classes = [AllowAny]

    def create(self, request):
        request.data["cpf"] = re.sub(r"\D", "", request.data["cpf"])
        request.data["telefone"] = re.sub(r"\D", "", request.data["telefone"])

        return super().create(request)


class RegistarPessoaJuridicaViewSet(CreateModelMixin, GenericViewSet):
    serializer_class = PessoaJuridicaSerializer
    permission_classes = [AllowAny]


class CustomTokenObtainPairView(TokenObtainPairView):
    serializer_class = MyTokenObtainPairSerializer
