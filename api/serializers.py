import re
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework import exceptions, serializers
from django.contrib.auth import authenticate
from api.models import (
    Precatorio,
    Endereco,
    EntidadePublica,
    PessoaFisica,
    PessoaJuridica,
    Usuario,
)


class UsuarioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Usuario
        fields = ["email", "password", "tipo"]
        extra_kwargs = {"password": {"write_only": True, "min_length": 6}}

    def create(self, validated_data):
        user = Usuario.objects.create_user(**validated_data)
        return user

    def to_representation(self, instance):
        """Overriding to remove Password Field when returning Data"""
        ret = super().to_representation(instance)
        ret.pop("password", None)
        return ret


class PrecatorioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Precatorio
        fields = "__all__"
        read_only_fields = ["QteVisualizacoes"]


class EnderecoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Endereco
        fields = "__all__"


class EntidadePublicaSerializer(serializers.ModelSerializer):
    class Meta:
        model = EntidadePublica
        fields = "__all__"


class PessoaFisicaSerializer(UsuarioSerializer):
    # user = UsuarioSerializer()
    endereco = serializers.PrimaryKeyRelatedField(queryset=Endereco.objects.all())

    class Meta:
        model = PessoaFisica
        exclude = [
            "last_login",
            "is_superuser",
            "first_name",
            "last_name",
            "date_joined",
            "groups",
            "user_permissions",
            "is_staff",
            "is_active",
        ]
        extra_kwargs = {"password": {"write_only": True, "min_length": 6}}

    def create(self, validated_data):
        # endereco_data = validated_data.pop("endereco", None)
        # endereco_created = Endereco.objects.create(**endereco_data)
        # pf = PessoaFisica.objects.create(
        #     user=endereco_created,
        #     cpf=validated_data["cpf"],
        #     dtNascimento=validated_data["dtNascimento"],
        #     sexo=validated_data["sexo"],
        #     profissao=validated_data["profissao"],
        # )
        pf = PessoaFisica.objects.create_user(**validated_data)

        return pf


class PessoaJuridicaSerializer(serializers.ModelSerializer):
    class Meta:
        model = PessoaJuridica
        fields = ("cnpj", "razao_social")

    def create(self, validated_data):
        user_data = validated_data.pop("user", None)
        user_created = Usuario.objects.create_user(**user_data)
        pj = PessoaJuridica.objects.create(
            user=user_created,
            cnpj=validated_data["cnpj"],
            razao_social=validated_data["razao_social"],
        )

        return pj


class MyTokenObtainPairSerializer(TokenObtainPairSerializer):
    username_field = "email"

    def validate(self, attrs):
        credentials = {"email": attrs.get("email"), "password": attrs.get("password")}

        user = authenticate(**credentials)

        if user:
            if not user.is_active:
                raise exceptions.AuthenticationFailed("User is deactivated")

            data = {}
            refresh = self.get_token(user)

            # data["refresh"] = str(refresh)
            data["access"] = str(refresh.access_token)

            return data
        else:
            raise exceptions.AuthenticationFailed(
                "No active account found with the given credentials"
            )

    @classmethod
    def get_token(cls, user):
        token = super().get_token(user)

        # Add custom claims
        token["email"] = user.email
        token["actort"] = user.tipo
        token["nameid"] = user.id

        return token
