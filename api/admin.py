from django.contrib import admin
from .models import (
    Precatorio,
    Usuario,
)

admin.site.register(Precatorio)
admin.site.register(Usuario)
