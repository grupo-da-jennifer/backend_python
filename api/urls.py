from rest_framework.routers import DefaultRouter
from django.urls import path
from api.views import (
    EnderecoViewSet,
    PrecatorioViewSet,
    PessoaFisicaViewSet,
    PessoaJuridicaViewSet,
    EntidadePublicaViewSet,
    RegistarPessoaFisicaViewSet,
    CustomTokenObtainPairView,
    RegistarPessoaJuridicaViewSet,
)


app_name = "api"

router = DefaultRouter(trailing_slash=False)
router.register(r"precatorio", PrecatorioViewSet)
router.register(r"pessoafisica", PessoaFisicaViewSet)
router.register(r"pessoajuridica", PessoaJuridicaViewSet)
router.register(r"entidadepublica", EntidadePublicaViewSet)
router.register(r"endereco", EnderecoViewSet)


urlpatterns = [
    path(
        "registrar/pessoafisica",
        RegistarPessoaFisicaViewSet.as_view({"post": "create"}),
        name="registrar_pessoa_fisica",
    ),
    path(
        "registrar/pessoajuridica",
        RegistarPessoaJuridicaViewSet.as_view({"post": "create"}),
        name="registrar_pessoa_juridica",
    ),
    path("login", CustomTokenObtainPairView.as_view(), name="token_obtain_pair"),
]

urlpatterns += router.urls
