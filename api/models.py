from django.conf import settings
from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.models import AbstractUser, Group, Permission
from djmoney.models.fields import MoneyField
from django.db import models
from utils import ESFERA, SEXO, TIPO_LOGRADOURO, UF, NATUREZA, TIPO_USUARIO


class ModeloBase(models.Model):
    """
    Classe que salva as datas de criação e atualização desse modelo.
    """

    # Salvando automaticamente as datas de criação e atualização dos dados
    dataPublicacao = models.DateTimeField(auto_now_add=True)
    dataAtualizacao = models.DateTimeField(auto_now=True)  ## MODIFICADO ########

    class Meta:
        """
        Classe que especifica que a classe a ModeloBase é abstrata
        """

        abstract = True


class ManagerUsuario(BaseUserManager):
    """
    Gerenciador de modelo de usuário personalizado em que email são os identificadores
    exclusivos para autenticação em vez de nomes de usuário.
    """

    def create_user(self, email, password, **extra_fields):
        """
        Create and save a User with the given email and password.
        """
        if not email:
            raise ValueError("O email é obrigatório")
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email, password, **extra_fields):
        """
        Create and save a SuperUser with the given email and password.
        """
        extra_fields.setdefault("is_staff", True)
        extra_fields.setdefault("is_superuser", True)
        extra_fields.setdefault("is_active", True)

        if extra_fields.get("is_staff") is not True:
            raise ValueError("Superuser must have is_staff=True.")
        if extra_fields.get("is_superuser") is not True:
            raise ValueError("Superuser must have is_superuser=True.")
        return self.create_user(email, password, **extra_fields)


class Usuario(AbstractUser):
    username = None
    password = models.CharField("password", max_length=128)
    email = models.EmailField(unique=True)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    tipo = models.CharField(max_length=2, choices=TIPO_USUARIO, null=True, blank=False)

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []

    objects = ManagerUsuario()
    groups = models.ManyToManyField(
        Group,
        related_name="usuario_%(class)s",
        blank=True,
    )
    user_permissions = models.ManyToManyField(
        Permission,
        related_name="usuario_%(class)s",
        blank=True,
    )

    def __str__(self):
        return self.email


class Endereco(ModeloBase):
    cep = models.CharField(max_length=8)
    descricaoLogradouro = models.CharField(max_length=100)
    numero = models.IntegerField()
    complemento = models.CharField(max_length=50)
    tipoLogradouro = models.CharField(max_length=50, choices=TIPO_LOGRADOURO)
    bairro = models.CharField(max_length=50)
    municipio = models.CharField(max_length=100)
    uf = models.CharField(max_length=2, choices=UF)

    def __str__(self):
        return f"{self.descricaoLogradouro} {self.numero} {self.bairro}"

    class Meta:
        verbose_name_plural = "Endereços"


class PessoaFisica(ModeloBase, Usuario):
    nome = models.CharField(max_length=200, null=True)
    cpf = models.CharField(max_length=11)
    dtNascimento = models.DateField()
    sexo = models.CharField(max_length=15, choices=SEXO, null=True)
    profissao = models.CharField(max_length=256, null=True)
    telefone = models.CharField(max_length=13, null=True)
    endereco = models.ForeignKey(Endereco, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.user.email

    class Meta:
        verbose_name_plural = "Pessoas Físicas"


class PessoaJuridica(ModeloBase, Usuario):
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name="pessoa_juridica_%(class)s",
    )
    cnpj = models.CharField(max_length=14)
    razao_social = models.CharField(max_length=100)

    def __str__(self):
        return self.user.email

    class Meta:
        verbose_name_plural = "Pessoas Jurídicas"


class EntidadePublica(ModeloBase):
    nome = models.CharField(max_length=256)
    cnpj = models.CharField(max_length=14)
    esfera = models.CharField(max_length=50, choices=ESFERA)
    endereco = models.ForeignKey(Endereco, on_delete=models.CASCADE)

    def __str__(self):
        return self.nome

    class Meta:
        verbose_name_plural = "Entidades Públicas"


class Precatorio(ModeloBase):
    aceito = models.BooleanField()
    ativo = models.BooleanField()
    anoPagamento = models.IntegerField()
    credor = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True, blank=False
    )
    dataEmissao = models.DateField()
    descricao = models.TextField()
    desagioPercentual = models.DecimalField(max_digits=5, decimal_places=2)
    mesPagamento = models.CharField(max_length=15)
    natureza = models.CharField(max_length=50, choices=NATUREZA)
    numero = models.IntegerField()
    # orgao_devedor = models.ForeignKey(EntidadePublica, on_delete=models.CASCADE)
    esfera = models.CharField(max_length=50, choices=ESFERA)
    orgao_devedor = models.CharField(max_length=50)
    QteVisualizacoes = models.IntegerField(default=0)
    rpv = models.BooleanField()
    valor = MoneyField(max_digits=14, decimal_places=2, default_currency="BRL")

    def __str__(self):
        return str(self.numero)

    class Meta:
        verbose_name_plural = "Precatórios"
